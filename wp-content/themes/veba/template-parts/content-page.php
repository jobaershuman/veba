<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package veba
 */

?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
		) !important;">

		<?php if( get_field('banner_title') ): ?>
			<div class="container">
				<div class="banner_cap">
					<div class="page_title">
						<h1><?php the_field('banner_title'); ?></h1>
						<p><?php the_field('caption_description'); ?></p>
					</div>
				</div><!-- /banner_cap -->
			</div>
		<?php endif; ?>
	</section>

<div class="container">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content page_dft_content">

		<div class="veba_sec_desc">
			<div class="row display_flex">
				<div class="col-sm-9">
					<div class="veba_page_title">
						<h1><?php the_title(); ?></h1>
					</div>

					<?php
						the_content();
					?>
				</div>
				<div class="col-sm-3 display_flex">
					<div class="sidebar">
						
						<?php get_sidebar(); ?>
						
					</div><!-- /sidebar -->
				</div>
			</div>
		</div>


		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'veba' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					esc_html__( 'Edit %s', 'veba' ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</div>