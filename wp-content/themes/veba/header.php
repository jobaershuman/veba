<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veba
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri();?>/js/html5shiv.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/respond.min.js"></script>
	<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'veba' ); ?></a>

		<header id="header">
			<div class="head">
				<div class="container">
					<div class="logo">

						<a href="<?php echo site_url();?>">
							<?php if( vp_veba_option('header_logo') ): ?>
								<img src="<?php echo vp_veba_option('header_logo');?>" alt="veba"></a>
							<?php else: ?>
								<img src="<?php echo get_stylesheet_directory_uri();?>/images/logo_veba.jpg" alt="veba"></a>
							<?php endif; ?>
						</a>
						
					</div>

					<div class="login_access">
						<a class="veba_login" href="#">Log-in Access</a>
					</div>
				</div>
			</div><!-- /head -->

			<div class="main_menu">
				<div class="container">
					<div class="menubar">
						<nav class="navbar navbar-default" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive_nav">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse" id="responsive_nav">
								<?php wp_nav_menu( array( 'theme_location' =>'main_menu', 'container' =>'', 'menu_class' =>'nav navbar-nav' ) ); ?>
							</div>
						</nav>
					</div>
				</div>
			</div><!-- /main_menu -->
		</header><!-- /header -->

		<div id="content" class="site-content">
