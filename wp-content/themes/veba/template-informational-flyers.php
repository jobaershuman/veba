<?php
/**
 * Template Name: Informational Flyers
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
		) !important;">

		<?php if( get_field('banner_title') ): ?>
			<div class="container">
				<div class="banner_cap">
					<div class="page_title">
						<h1><?php the_field('banner_title'); ?></h1>
						<p><?php the_field('caption_description'); ?></p>
					</div>
				</div><!-- /banner_cap -->
			</div>
		<?php endif; ?>
	</section>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					<section class="sec_menu">
						<div class="container">
							<div class="sec_menu_list">
								<ul>
									<li class="active"><a href="#">Informational flyers</a></li>
									<li><a href="#">events</a></li>
									<li><a href="#">HR Resources</a></li>
									<li><a href="#">Contact info</a></li>
								</ul>
							</div>
						</div>
					</section>

					<section class="pad_tb100">
						<div class="container">
							<div id="informational_flyers" class="sec_content">
								<div class="veba_sec_desc">
									<div class="row display_flex">
										<div class="col-sm-9">
											<?php if( get_field('page_section_title') ): ?>
												<div class="veba_sec_title bdr_none">
														<h1><?php the_field('page_section_title'); ?></h1>
												</div>
											<?php endif; ?>
											
											<div class="info_flyer_content">

												<div class="row display_flex">
													<?php 
														$counter = 0;
														$veba_flyer = new WP_Query(array(
															'post_type' => 'flyer',
															"post_status"	=> 'publish',
															'posts_per_page' => '-1'
														));
													?>

													<?php while( $veba_flyer->have_posts() ) : $veba_flyer->the_post(); ?>
														<?php if ($counter && $counter % 3 === 0){
															echo '</div><div class="row display_flex">';
														} $counter++; ?>

														<div class="col-sm-4">
															<div class="flyer_item">
																<div class="item_pic">
																	<?php if ( has_post_thumbnail() ) { ?>
																		<a href="<?php the_permalink(); ?>"> 
																			<?php the_post_thumbnail(); ?> 
																		</a>
																	<?php } else { ?>
																		<img src="http://placehold.it/200x283/fbd1d2/fff?text=No+Image" alt="news_img">
																	<?php } ?>
																</div>
																<div class="flyer_name">
																	<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
																</div>
															</div><!-- /flyer_item -->
														</div>
													<?php endwhile; ?>
												</div><!-- /row -->

											</div>
										</div>

										<div class="col-sm-3 display_flex">
											<div class="sidebar">

												<?php get_sidebar(); ?>

											</div><!-- /sidebar -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					
				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>