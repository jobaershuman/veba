<?php
/**
 * Template Name: Veba Members
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
		) !important;">

		<?php if( get_field('banner_title') ): ?>
			<div class="container">
				<div class="banner_cap">
					<div class="page_title">
						<h1><?php the_field('banner_title'); ?></h1>
						<p><?php the_field('caption_description'); ?></p>
					</div>
				</div><!-- /banner_cap -->
			</div>
		<?php endif; ?>
	</section>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					<section class="pad_tb100 school_info">
						<div class="container">
							<div class="sch_info_container">
								<div class="veba_sec_title">
									<h1><?php the_field('membar_top_section_title'); ?></h1>
								</div>

								<div class="veba_sec_desc">
									<div class="sch_asn_docs members_container">
										<div class="row display_flex">

											<?php if( get_field('member_type_1') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_1'); ?></h2>
													</div>
													<a href="<?php the_field('type_1_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>


											<?php if( get_field('member_type_2') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_2'); ?></h2>
													</div>
													<a href="<?php the_field('type_2_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>
											

											<?php if( get_field('member_type_3') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_3'); ?></h2>
													</div>
													<a href="<?php the_field('type_3_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>
										</div><!-- /row -->

										<div class="row"><hr></div>

										<div class="row display_flex">

											<?php if( get_field('member_type_4') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_4'); ?></h2>
													</div>
													<a href="<?php the_field('type_4_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>

											<?php if( get_field('member_type_5') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_5'); ?></h2>
													</div>
													<a href="<?php the_field('type_5_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>

											<?php if( get_field('member_type_6') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('member_type_6'); ?></h2>
													</div>
													<a href="<?php the_field('type_6_button_link'); ?>" class="button">Click Here</a>
												</div>
											</div>
											<?php endif; ?>

										</div><!-- /row -->
									</div>
								</div>
							</div>
						</div>
					</section>

					<?php if( get_field('wellness_newsletter_text') ): ?>
						<section class="news_latter_sec">
							<div class="container">
								<div class="intro_desc">
									<p><?php the_field('wellness_newsletter_text'); ?></p>
								</div>
							</div>
						</section><!-- /news_latter_sec -->
					<?php endif; ?>

				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>