<?php
/**
 * veba functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package veba
 */

if ( ! function_exists( 'veba_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function veba_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on veba, use a find and replace
	 * to change 'veba' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'veba', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main_menu' 			=> __( 'Main Menu', 'veba' ),
		'footer_menu' 			=> __( 'Footer Menu', 'veba' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'veba_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'veba_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function veba_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'veba_content_width', 640 );
}
add_action( 'after_setup_theme', 'veba_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function veba_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'veba' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 1', 'veba' ),
		'id'            => 'footer-section-1',
		'description'   => esc_html__( 'Add Veba News Letter Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget footer_item %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="footer_title"><h2>',
		'after_title'   => '</h2></div>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 2', 'veba' ),
		'id'            => 'footer-section-2',
		'description'   => esc_html__( 'Add Veba Office Address Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget footer_item %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="footer_title"><h2>',
		'after_title'   => '</h2></div>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 3', 'veba' ),
		'id'            => 'footer-section-3',
		'description'   => esc_html__( 'Add Footer Menu Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget footer_item %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="footer_title"><h2>',
		'after_title'   => '</h2></div>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Footer Section 4', 'veba' ),
		'id'            => 'footer-section-4',
		'description'   => esc_html__( 'Add Veba Results Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget footer_item %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<div class="footer_title"><h2>',
		'after_title'   => '</h2></div>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'Contact Us Sidebar', 'veba' ),
		'id'            => 'contact-us',
		'description'   => esc_html__( 'Add Contact Us Description Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );


	register_sidebar( array(
		'name'          => esc_html__( 'School Info Register Form', 'veba' ),
		'id'            => 'school-info-form',
		'description'   => esc_html__( 'Add School Info Register Form Widgets Here.', 'veba' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'veba_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function veba_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.css' );
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css' );
	wp_enqueue_style( 'custom-fonts', get_template_directory_uri() . '/css/fonts.css' );
	wp_enqueue_style( 'owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css' );

	wp_enqueue_style( 'veba-style', get_stylesheet_uri() );
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/responsive.css' );

	//wp_enqueue_script( 'veba-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20170530', true );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '20170530', true );

	wp_enqueue_script( 'jquery.sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array(), '20170530', true );
	wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array(), '20170530', true );
	wp_enqueue_script( 'owl.carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20170530', true );

	wp_enqueue_script( 'veba-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20170530', true );

	wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '20170530', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'veba_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * CMB2 Custom Metabox Functions
 */
require get_template_directory() . '/inc/custom-metabox.php';



/**
 * Custom Vafpress Options Framework
 */
require_once get_template_directory() . '/inc/vafpress/bootstrap.php';


/**
 * Create instance of Options
 */

$tmpl_opt  = get_template_directory() . '/inc/option.php';

$theme_options = new VP_Option(
	array(
		'is_dev_mode'           => false,                                  	// dev mode, default to false
		'option_key'            => 'vp_veba_option',                      	// options key in db, required
		'page_slug'             => 'vp_veba_option',                      	// options page slug, required
		'template'              => $tmpl_opt,                              	// template file path or array, required
		'menu_page'             => 'themes.php',                           	// parent menu slug or supply `array` (can contains 'icon_url' & 'position') for top level menu
		'use_auto_group_naming' => true,                                  	// default to true
		'use_util_menu'         => true,                                   	// default to true, shows utility menu
		'minimum_role'          => 'edit_theme_options',                   	// default to 'edit_theme_options'
		'layout'                => 'fixed',                                	// fluid or fixed, default to fixed
		'page_title'            => __( 'Theme Options', 'veba' ),  	   		// page title
		'menu_label'            => __( 'Theme Options', 'veba' ),  	   		// menu label
		)
);


function vp_veba_option( $name ){
	return vp_option( "vp_veba_option." . $name );
}




/* Become A Partner */
$labels = array(
	"name"          => "Flyer",
	"singular_name" => "Flyer",
	"menu_name"     => "Flyer",
	);

$args = array(
	"labels"              => $labels,
	"description"         => "Flyer",
	"public"              => true,
	"show_ui"             => true,
	"has_archive"         => true,
	"show_in_menu"        => true,
	"exclude_from_search" => true,
	"capability_type"     => "post",
	"map_meta_cap"        => true,
	"hierarchical"        => false,
	"rewrite"             => array( "slug" => "flyer", "with_front" => true ),
	"query_var"           => true,
	"supports"            => array( "title", "editor", "thumbnail" ),
	);
register_post_type( "flyer", $args );




/* custom_excerpt */
function custom_excerpt(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	// $excerpt = substr($excerpt, 0, 40);
	// $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	// $excerpt = $excerpt.'...';
	return $excerpt;
}

function truncate($input, $maxWords, $maxChars=500){
    $words = preg_split('/\s+/', $input);
    $words = array_slice($words, 0, $maxWords);
    $words = array_reverse($words);

    $chars = 0;
    $truncated = array();

    while(count($words) > 0)
    {
        $fragment = trim(array_pop($words));
        $chars += strlen($fragment);

        if($chars > $maxChars) break;

        $truncated[] = $fragment;
    }

    $result = implode($truncated, ' ');

    return $result . ($input == $result ? '' : ' ...');
}


