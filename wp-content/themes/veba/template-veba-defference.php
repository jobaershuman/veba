<?php
/**
 * Template Name: Veba Defference
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
		) !important;">

		<?php if( get_field('banner_title') ): ?>
			<div class="container">
				<div class="banner_cap">
					<div class="page_title">
						<h1><?php the_field('banner_title'); ?></h1>
						<p><?php the_field('caption_description'); ?></p>
					</div>
				</div><!-- /banner_cap -->
			</div>
		<?php endif; ?>
	</section>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div id="defference_page" class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					
					<section class="about_block">
						<div class="container">
							<div class="row display_flex">
								<div class="col-md-5 col-sm-6">
									<div class="about_block_title">
										<div class="sec_icon">
											<img src="<?php the_field('section_1_icon'); ?>" alt="icon">
										</div>
										<h1><?php the_field('section_1_title'); ?></h1>
									</div>
								</div>
								<div class="col-md-7 col-sm-6">
									<div class="about_block_desc">
										<div class="post_title">
											<h2><?php the_field('section_1_description_title'); ?></h2>
										</div>
										<div class="post_desc">
											<p><?php the_field('section_1_description'); ?></p>
											<a href="<?php the_field('button_1_link'); ?>" class="button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section><!-- /about_block -->

					<section class="about_block row_rtl">
						<div class="container">
							<div class="row">
								<div class="col-md-5 col-sm-6">
									<div class="about_block_title">
										<div class="sec_icon">
											<img src="<?php the_field('section_2_icon'); ?>" alt="icon">
										</div>
										<h1><?php the_field('section_2_title'); ?></h1>
									</div>
								</div>
								<div class="col-md-7 col-sm-6">
									<div class="about_block_desc">
										<div class="post_title">
											<h2><?php the_field('section_2_description_title'); ?></h2>
										</div>
										<div class="post_desc">
											<p><?php the_field('section_2_description'); ?></p>
											<a href="<?php the_field('button_2_link'); ?>" class="button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section><!-- /about_block/row_rtl -->

					<section class="about_block">
						<div class="container">
							<div class="row display_flex">
								<div class="col-md-5 col-sm-6">
									<div class="about_block_title">
										<div class="sec_icon">
											<img src="<?php the_field('section_3_icon'); ?>" alt="icon">
										</div>
										<h1><?php the_field('section_3_title'); ?></h1>
									</div>
								</div>
								<div class="col-md-7 col-sm-6">
									<div class="about_block_desc">
										<div class="post_title">
											<h2><?php the_field('section_3_description_title'); ?></h2>
										</div>
										<div class="post_desc">
											<p><?php the_field('section_3_description'); ?></p>
											<a href="<?php the_field('button_3_link'); ?>" class="button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section><!-- /about_block -->

					<section class="about_block row_rtl">
						<div class="container">
							<div class="row">
								<div class="col-md-5 col-sm-6">
									<div class="about_block_title">
										<div class="sec_icon">
											<img src="<?php the_field('section_4_icon'); ?>" alt="icon">
										</div>
										<h1><?php the_field('section_4_title'); ?></h1>
									</div>
								</div>
								<div class="col-md-7 col-sm-6">
									<div class="about_block_desc">
										<div class="post_title">
											<h2><?php the_field('section_4_description_title'); ?></h2>
										</div>
										<div class="post_desc">
											<p><?php the_field('section_4_description'); ?></p>
											<a href="<?php the_field('button_4_link'); ?>" class="button">Learn More</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section><!-- /about_block/row_rtl -->
					

				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>