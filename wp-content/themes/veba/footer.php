<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package veba
 */

?>

	</div><!-- #content -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-3">
					<?php dynamic_sidebar( 'footer-section-1' ); ?>
				</div><!-- /footer_item -->

				<div class="col-lg-4 col-md-3 col-sm-3">
					<?php dynamic_sidebar( 'footer-section-2' ); ?>
				</div><!-- /footer_item -->

				<div class="col-lg-2 col-md-2 col-sm-3">
					<?php dynamic_sidebar( 'footer-section-3' ); ?>
				</div><!-- /footer_item -->

				<div class="col-lg-2 col-md-3 col-sm-3">
					<?php dynamic_sidebar( 'footer-section-4' ); ?>
				</div><!-- /footer_item -->
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
