<?php
/**
 * Template Name: School District Info
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
		) !important;">

		<?php if( get_field('banner_title') ): ?>
			<div class="container">
				<div class="banner_cap">
					<div class="page_title">
						<h1><?php the_field('banner_title'); ?></h1>
						<p><?php the_field('caption_description'); ?></p>
					</div>
				</div><!-- /banner_cap -->
			</div>
		<?php endif; ?>
	</section>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					<section class="pad_tb100 school_info">
						<div class="container">
							<div class="sch_info_container">
								<div class="veba_sec_title">
									<h1><?php the_field('section_top_title'); ?></h1>
								</div>

								<div class="veba_sec_desc">
									<div class="row">
										<div class="col-sm-4">
											<div class="school_sidebar">
												<div class="school_login">
													<?php dynamic_sidebar( 'school-info-form' ); ?>
												</div>
											</div>
										</div><!-- /help_item -->

										<div class="col-sm-8">
											<div class="sch_asn_docs">
												<div class="row">
													<div class="col-sm-6">
														<div class="help_item">
															<div class="help_item_title">
																<h2><?php the_field('info_1_title'); ?></h2>
															</div>
															<a href="<?php the_field('info_1_button_link'); ?>" class="button">Click Here <i class="fa fa-lock" aria-hidden="true"></i></a>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="help_item unlock_event">
															<div class="help_item_title">
																<h2><?php the_field('info_2_title'); ?></h2>
															</div>
															<a href="<?php the_field('info_2_button_link'); ?>" class="button">Click Here <i class="fa fa-lock" aria-hidden="true"></i></a>
														</div>
													</div>
												</div><!-- /row -->

												<div class="row"><hr></div>

												<div class="row">
													<div class="col-sm-6">
														<div class="help_item">
															<div class="help_item_title">
																<h2><?php the_field('info_3_title'); ?></h2>
															</div>
															<a href="<?php the_field('info_3_button_link'); ?>" class="button">Click Here <i class="fa fa-lock" aria-hidden="true"></i></a>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="help_item">
															<div class="help_item_title">
																<h2><?php the_field('info_4_title'); ?></h2>
															</div>
															<a href="<?php the_field('info_4_button_link'); ?>" class="button">Click Here <i class="fa fa-lock" aria-hidden="true"></i></a>
														</div>
													</div>
												</div><!-- /row -->
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</section>
					

				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>