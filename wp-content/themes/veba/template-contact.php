<?php
/**
 * Template Name: Contact Page
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

	<section class="banner" style="background-image: url(
		<?php if ( get_field('banner_image') ) { 
			the_field('banner_image');
		} else { ?>
			<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
		<?php } ?>
	) !important;">

		<div class="container">
			<div class="banner_cap">
				<div class="page_title">
					<h1><?php the_field('banner_title'); ?></h1>
					<p><?php the_field('caption_description'); ?></p>
				</div>
			</div><!-- /banner_cap -->
		</div>
		
	</section>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					<section class="pad_tb100 contact">
						<div class="container">
							<div class="contact_content">
								<div class="veba_sec_desc">
									<div class="row display_flex">
										<div class="col-sm-9">
											<?php the_content(); ?>
										</div>
										<div class="col-sm-3 display_flex">
											<div class="contact_sidebar">
												<?php dynamic_sidebar( 'contact-us' ); ?>
											</div><!-- /contact_sidebar -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					

				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>