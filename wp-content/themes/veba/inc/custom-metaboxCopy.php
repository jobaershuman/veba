<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/CMB2/init.php';
}


//Front Page Banner
add_action( 'cmb2_admin_init', 'frontpage_banner_meta' );

function frontpage_banner_meta() {

    $prefix = '_banner_cap_';

    $cmb = new_cmb2_box( array(
        'id'            => 'frontpage_banner_meta',
        'title'         => __( 'Banner Option', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-frontpage.php' ),
        'show_names'    => true,
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Banner Title', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'title',
        'type'       => 'textarea',
        'attributes'  => array(
            'placeholder' => 'Power of Many',
            'rows'        => 2,
        ),
        'show_on_cb' => 'cmb2_hide_if_no_cats', 
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Banner Caption Button Text', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'button_text',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', 
    ) );

    $cmb->add_field( array(
        'name' => __( 'Banner Button Link', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'btn_url',
        'type' => 'text_url',
    ) );

}


//Front Page Promo
add_action( 'cmb2_admin_init', 'frontpage_promo_meta' );

function frontpage_promo_meta() {

    $prefix = '_promo_';

    $cmb = new_cmb2_box( array(
        'id'            => 'frontpage_promo_meta',
        'title'         => __( 'Home Promo Option', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-frontpage.php' ),
        'show_names'    => true,
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name' => __( 'Promo White Circle Title', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'circle_lg_title',
        'type' => 'textarea_code',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'attributes'  => array(
            'placeholder' => '',
            'rows'        => 2,
        ),
    ) );

    $cmb->add_field( array(
        'name' => __( 'White Circle Small Text', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'white_circle_text',
        'type' => 'textarea_code',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'attributes'  => array( 'rows' => 2, ),
    ) );

    //Small 4 Circle field
    $cmb->add_field( array(
        'name' => __( 'Small Circle Tile 1', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'circle_sm_title1',
        'type' => 'text_medium',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Small Circle Tile 2', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'circle_sm_title2',
        'type' => 'text_medium',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Small Circle Tile 3', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'circle_sm_title3',
        'type' => 'text_medium',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Small Circle Tile 4', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'circle_sm_title4',
        'type' => 'text_medium',
        'attributes' => array(
            'type' => 'text',
        ),
    ) );

}

//Gatway Paage Enter Box Options
add_action( 'cmb2_admin_init', 'gatway_enter_meta' );

function gatway_enter_meta() {

    $prefix = '_gtw_enter_';

    $cmb = new_cmb2_box( array(
        'id'            => 'gatway_enter_meta',
        'title'         => __( 'Gatway Box 1 Enter Options', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-gateway.php' ),
        'show_names'    => true,
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Box 1 Title', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'box_title',
        'type'       => 'text',
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Box 1 Description', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'box_desc',
        'type'       => 'textarea',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Upload sponsor Logo', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'sponsor_logo',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Gatway Enter Button Link', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'button_link',
        'type' => 'text_url',
    ) );

}

//Gatway Paage Visit Box Options
add_action( 'cmb2_admin_init', 'gatway_visit_meta' );

function gatway_visit_meta() {

    $prefix = '_gtw_visit_';

    $cmb = new_cmb2_box( array(
        'id'            => 'gatway_visit_meta',
        'title'         => __( 'Gatway Box 2 Visit Options', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-gateway.php' ),
        'show_names'    => true,
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name'       => __( 'Box 2 Title', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'box_title',
        'type'       => 'text',
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Box 2 Description', 'cmb2' ),
        'desc'       => __( '', 'cmb2' ),
        'id'         => $prefix . 'box_desc',
        'type'       => 'textarea',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Upload sponsor Logo', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'sponsor_logo',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Gatway Visit Button Link', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'button_link',
        'type' => 'text_url',
    ) );

}


//Gatway Paage Visit Box Options
add_action( 'cmb2_admin_init', 'overview_page_meta' );

function overview_page_meta() {

    $prefix = '_ovw_';

    $cmb = new_cmb2_box( array(
        'id'            => 'overview_page_meta',
        'title'         => __( 'Overview Paage Options', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-overview.php' ),
        'show_names'    => true,
    ) );

    // Regular text field
    $cmb->add_field( array(
        'name' => __( 'First Section Content', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'ovw_section1',
        'type' => 'wysiwyg',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'options' => array( 
            'textarea_rows' => 4,
            'wpautop' => true,
            ),
    ) );

    $cmb->add_field( array(
        'name' => __( 'Second Section Content', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'ovw_section2',
        'type' => 'wysiwyg',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'options' => array( 
            'textarea_rows' => 4,
            'wpautop' => true,
            ),
    ) );

    $cmb->add_field( array(
        'name' => __( 'Third Section Content', 'cmb2' ),
        'desc' => __( '', 'cmb2' ),
        'id'   => $prefix . 'ovw_section3',
        'type' => 'wysiwyg',
        'sanitization_cb' => 'prefix_sanitize_text_callback',
        'options' => array( 
            'textarea_rows' => 4,
            'wpautop' => true,
            ),
    ) );
}



//Slider Meta
add_action( 'cmb2_init', 'repeatable_slider_metabox' );

function repeatable_slider_metabox() {

    $prefix = '_home_slider_';

    $cmb_group = new_cmb2_box( array(
        'id'            => $prefix . 'pp_slider_meta',
        'title'         => __( 'Slider Group', 'cmb2' ),
        'object_types'  => array( 'page', ),
        'show_on'      => array( 'key' => 'page-template', 'value'=>'template-frontpage.php' ),
    ) );

    // Parent
    $group_field_id = $cmb_group->add_field( array(
        'id'          => $prefix . 'slide_images_group',
        'type'        => 'group',
        'options'     => array(
            'group_title'   => __( 'Slider {#}', 'cmb2' ), 
            'add_button'    => __( 'Add Another Slider', 'cmb2' ),
            'remove_button' => __( 'Remove Slider', 'cmb2' ),
            'sortable'      => true, // beta
        ),
    ) );

    // Slider Image Field
    $cmb_group->add_group_field( $group_field_id, array(
        'name'        => __( 'Slider Images', 'cmb2' ),
        'id'          => 'pp_slider_img',
        'type'        => 'file',
    ) );

}



