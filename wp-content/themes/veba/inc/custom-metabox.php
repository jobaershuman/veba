<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/CMB2/init.php';
}




//Slider Meta
// add_action( 'cmb2_init', 'repeatable_slider_metabox' );

// function repeatable_slider_metabox() {

//     $prefix = '_home_slider_';

//     $cmb_group = new_cmb2_box( array(
//         'id'            => $prefix . 'pp_slider_meta',
//         'title'         => __( 'Slider Group', 'cmb2' ),
//         'object_types'  => array( 'page', ),
//         'show_on'      => array( 'key' => 'page-template', 'value'=>'template-frontpage.php' ),
//     ) );

//     // Parent
//     $group_field_id = $cmb_group->add_field( array(
//         'id'          => $prefix . 'slide_images_group',
//         'type'        => 'group',
//         'options'     => array(
//             'group_title'   => __( 'Slider {#}', 'cmb2' ), 
//             'add_button'    => __( 'Add Another Slider', 'cmb2' ),
//             'remove_button' => __( 'Remove Slider', 'cmb2' ),
//             'sortable'      => true, // beta
//         ),
//     ) );

//     // Slider Image Field
//     $cmb_group->add_group_field( $group_field_id, array(
//         'name'        => __( 'Slider Images', 'cmb2' ),
//         'id'          => 'pp_slider_img',
//         'type'        => 'file',
//     ) );

// }



