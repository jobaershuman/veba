<?php
return array(
	'title' => __('Veba Option Panel', 'veba'),
	'logo'  => get_template_directory_uri() . '/images/logo_sm.png',
	'menus' => array(
		array( //General Options
			'title' => __('General Options', 'veba'),
			'name' => 'general_options',
			'icon' => 'font-awesome:fa-cog',
			'controls' => array(
				array(
					'type' => 'section',
					'title' => __('Veba Logo', 'veba'),
					'fields' => array(
						array(
							'type' => 'upload',
							'name' => 'header_logo',
							'label' => __('Header Logo', 'veba'),
							'default' => get_template_directory_uri() . '/images/logo_veba.jpg',
						),
					),
				),

			)
		),
		array( //Custom Scripts
			'title' => __('Custom Scripts', 'veba'),
			'name' => 'custom_scripts',
			'icon' => 'font-awesome:fa-code',
			'controls' => array(
				array(
					'type' => 'codeeditor',
					'name' => 'custom_css',
					'label' => __('Custom CSS', 'veba'),
					'description' => __('Write your custom css here.', 'veba'),
					'theme' => 'github',
					'mode' => 'css',
				),
				array(
					'type' => 'codeeditor',
					'name' => 'custom_js',
					'label' => __('Custom JS', 'veba'),
					'description' => __('Write your custom js here.', 'veba'),
					'theme' => 'twilight',
					'mode' => 'javascript',
				),
			)
		),
		array( //Email Settings
			'title' => __('Email Settings', 'veba'),
			'name' => 'email_settings',
			'icon' => 'font-awesome:fa-envelope',
			'controls' => array(
				array(
					'type' => 'textbox',
					'name' => 'from_email_address',
					'label' => __('From Email Address', 'veba'),
					'default'=> get_option( 'admin_email' ),
					'validation'=>'email'
				),
				array(
					'type' => 'section',
					'title' => __('Signup Email', 'veba'),
					'fields' => array(
						array(
							'type' => 'textbox',
							'name' => 'signup_email_subject',
							'label' => __('Signup Email Subject', 'veba'),
							),
						array(
							'type' => 'textarea',
							'name' => 'signup_email_body',
							'label' => __('Signup Email Body', 'veba'),
							'description' => __('Placeholders: {name}, {email}, {username}, {password}, {login_link}', 'veba'),
						),
					)
				)
			)
		),
	)
);

/**
 *EOF
 */