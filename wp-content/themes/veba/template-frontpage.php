<?php
/**
 * Template Name: Haome Page
 *
 * This is the template that displays pages without sidebars.
 *
 * @package WordPress
 * @since veba 1.0
 */

get_header(); ?>

<section class="home_slider">
	<?php 
		$home_slider = new WP_Query(array(
			'post_type' => 'slider',
			"post_status"	=> 'publish',
			'posts_per_page' => '-1'
		));
	?>
	<div id="homeSlider" class="owl-carousel">
		<?php while( $home_slider->have_posts() ) : $home_slider->the_post(); ?>
										
            <div class="item" style="background-image: url(
				<?php if ( has_post_thumbnail() ) { 
					the_post_thumbnail_url();
				} else { ?>
					<?php echo get_template_directory_uri() . '/images/banner_img1.jpg' ?>
				<?php } ?>
			) !important;">

				<div class="container">
					<?php if( get_field('slider_title') ): ?>
					<div class="banner_cap">
						<div class="page_title">
							<h1><?php the_field('slider_title'); ?></h1>
							<p><?php the_field('slider_sub_title'); ?></p>
						</div>

						<?php if( get_field('slider_button_text') ): ?>
							<a class="button" href="<?php the_field('button_link'); ?>"><?php the_field('slider_button_text'); ?></a>
						<?php endif; ?>
					</div><!-- /banner_cap -->
					<?php endif; ?>
				</div>
            	
            </div><!-- /item -->

        <?php endwhile; ?>
	</div>
</section><!-- /home_slider -->


<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    
	    <div class="page_content">

	    	<?php if( have_posts() ): ?>
				<?php while (have_posts()): the_post(); ?>
					<?php //get_template_part( 'template-parts/content', get_post_format() ); ?>

					<section class="pad_tb100 hc_help">
						<div class="container">
							<div class="hc_help">

								<?php if( get_field('help_top_title') ): ?>
									<div class="hch_title">
										<h1><?php the_field('help_top_title'); ?></h1>
									</div>
								<?php endif; ?>

								<div class="hch_desc">
									<div class="row display_flex">

										<?php if( get_field('help_type_1') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('help_type_1'); ?></h2>
													</div>
													<a href="<?php the_field('help_type_1_link'); ?>" class="button">Click Here</a>
												</div>
											</div><!-- /help_item -->
										<?php endif ?>

										<?php if( get_field('help_type_2') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('help_type_2'); ?></h2>
													</div>
													<a href="<?php the_field('help_type_2_link'); ?>" class="button">Click Here</a>
												</div>
											</div><!-- /help_item -->
										<?php endif ?>

										<?php if( get_field('help_type_3') ): ?>
											<div class="col-sm-4">
												<div class="help_item">
													<div class="help_item_title">
														<h2><?php the_field('help_type_3'); ?></h2>
													</div>
													<a href="<?php the_field('help_type_3_link'); ?>" class="button">Click Here</a>
												</div>
											</div><!-- /help_item -->
										<?php endif ?>

									</div>
								</div>
							</div>
						</div>
					</section>

					<section class="pad_tb100 testimonial">
						<div class="testimonial sliderFull">
		                    <div class="container">

		                    	<?php 
									//$counter = 0;
									$testimonial_slider = new WP_Query(array(
										'post_type' => 'testimonial',
										"post_status"	=> 'publish',
										'posts_per_page' => '-1'
									));
								?>

			                    <div id="testimonial" class="owl-carousel">
			                    	<?php while( $testimonial_slider->have_posts() ) : $testimonial_slider->the_post(); ?>
										
				                        <div class="item">
			                            	<div class="tsm_desc">
												<?php the_content(); ?>
												<span class="author_name"><?php the_title(); ?></span>
											</div>
				                        </div>

			                        <?php endwhile; ?>
			                    </div><!-- /testimonial -->
		                    </div>
		                </div>
					</section>

				<?php endwhile; ?>
			<?php endif; ?>

	    </div><!-- /page_content -->

    </div><!-- #content -->
</div><!-- #primary --><!-- #content -->
<!-- #primary -->

<?php get_footer(); ?>