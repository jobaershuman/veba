
jQuery( document ).ready(function($) {

	new WOW({ mobile: false }).init();

	$(".main_menu").sticky({topSpacing:0});

    $('#testimonial').owlCarousel({
        loop: true,
        center: true,
        stagePadding: 0,
        smartSpeed: 1200,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                dots: false,
                nav: true,
                navText: [
                            " <i class='fa fa-angle-left' aria-hidden='true'></i> ", 
                            " <i class='fa fa-angle-right' aria-hidden='true'></i> "
                        ]
            }
        }
    });

    $('#homeSlider').owlCarousel({
        loop: true,
        center: true,
        stagePadding: 0,
        smartSpeed: 1200,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                dots: false,
                nav: true,
                navText: [
                			" <i class='fa fa-angle-left' aria-hidden='true'></i> ", 
                		  	" <i class='fa fa-angle-right' aria-hidden='true'></i> "
                		]
            }
        }
    });
	
});
